# proxmox_login_manager

A basic login management library which handles authentication in combination
with the proxmox_dart_api_client library.

You need to build the model classes before using it, to do this use:
```Bash
flutter packages pub run build_runner build
```
Basic usage:
```dart
ProxmoxLoginSelector(
    onLogin: (ProxmoxApiClient client) => whatever you want to do with the client,
),
```
This will give you an authenticated ProxmoxApiClient for usage in your project.

The latest session will be saved and can be recovered.
```dart
loginStorage = await ProxmoxLoginStorage.fromLocalStorage();
final apiClient = await loginStorage.recoverLatestSession();
```
This will either return an authenticated ProxmoxApiClient or will result in an
Exception (ProxmoxApiException).

To clear all session on logout you can use:
```dart
ProxmoxLoginStorage.fromLocalStorage()
                .then((storage) => storage?.invalidateAllSessions());
```