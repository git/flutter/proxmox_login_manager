import 'package:flutter/material.dart';
import 'package:proxmox_login_manager/proxmox_general_settings_model.dart';

class ProxmoxGeneralSettingsForm extends StatefulWidget {
  const ProxmoxGeneralSettingsForm({super.key});

  @override
  State<ProxmoxGeneralSettingsForm> createState() =>
      _ProxmoxGeneralSettingsFormState();
}

class _ProxmoxGeneralSettingsFormState
    extends State<ProxmoxGeneralSettingsForm> {
  Future<ProxmoxGeneralSettingsModel>? _settings;
  @override
  void initState() {
    super.initState();
    _settings = ProxmoxGeneralSettingsModel.fromLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: FutureBuilder<ProxmoxGeneralSettingsModel>(
          future: _settings,
          builder: (context, snaptshot) {
            if (snaptshot.hasData) {
              final settings = snaptshot.data!;
              return SingleChildScrollView(
                child: Column(
                  children: [
                    SwitchListTile(
                      title: const Text('Validate SSL connections'),
                      subtitle: const Text('e.g. validates certificates'),
                      value: settings.sslValidation!,
                      onChanged: (value) async {
                        await settings
                            .rebuild((b) => b.sslValidation = value)
                            .toLocalStorage();
                        setState(() {
                          _settings =
                              ProxmoxGeneralSettingsModel.fromLocalStorage();
                        });
                      },
                    )
                  ],
                ),
              );
            }

            return const Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }
}
