import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:proxmox_login_manager/serializers.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'proxmox_general_settings_model.g.dart';

abstract class ProxmoxGeneralSettingsModel
    implements
        Built<ProxmoxGeneralSettingsModel, ProxmoxGeneralSettingsModelBuilder> {
  bool? get sslValidation;
  BuiltList<String>? get trustedFingerprints;

  ProxmoxGeneralSettingsModel._();
  factory ProxmoxGeneralSettingsModel(
          [void Function(ProxmoxGeneralSettingsModelBuilder)? updates]) =
      _$ProxmoxGeneralSettingsModel;

  factory ProxmoxGeneralSettingsModel.defaultValues() =>
      ProxmoxGeneralSettingsModel((b) => b
        ..sslValidation = true
        ..trustedFingerprints = ListBuilder());

  Object toJson() {
    return serializers.serializeWith(
            ProxmoxGeneralSettingsModel.serializer, this) ??
        {};
  }

  static ProxmoxGeneralSettingsModel fromJson(Object? json) {
    return serializers.deserializeWith(
            ProxmoxGeneralSettingsModel.serializer, json) ??
        ProxmoxGeneralSettingsModel();
  }

  static Serializer<ProxmoxGeneralSettingsModel> get serializer =>
      _$proxmoxGeneralSettingsModelSerializer;

  static Future<ProxmoxGeneralSettingsModel> fromLocalStorage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('ProxmoxGeneralSettings')) {
      final decodedJson =
          json.decode(prefs.getString('ProxmoxGeneralSettings')!);
      return fromJson(decodedJson);
    }
    return ProxmoxGeneralSettingsModel.defaultValues();
  }

  Future<void> toLocalStorage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('ProxmoxGeneralSettings', json.encode(toJson()));
  }
}
