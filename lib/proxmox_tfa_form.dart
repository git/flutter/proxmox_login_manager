import 'package:flutter/material.dart';
import 'package:proxmox_dart_api_client/proxmox_dart_api_client.dart';
import 'package:proxmox_login_manager/proxmox_login_form.dart';

class ProxmoxTfaForm extends StatefulWidget {
  final ProxmoxApiClient? apiClient;

  const ProxmoxTfaForm({super.key, this.apiClient});

  @override
  State<ProxmoxTfaForm> createState() => _ProxmoxTfaFormState();
}

class _ProxmoxTfaFormState extends State<ProxmoxTfaForm> {
  final TextEditingController _codeController = TextEditingController();
  bool _isLoading = false;
  List<String> _tfaKinds = [];
  String _selectedTfaKind = "";

  @override
  void initState() {
    super.initState();
    _tfaKinds = widget.apiClient!.credentials.tfa!.kinds().toList();
    _selectedTfaKind = _tfaKinds[0];
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData.dark().copyWith(
          colorScheme: const ColorScheme.dark().copyWith(
              secondary: ProxmoxColors.orange,
              onSecondary: ProxmoxColors.supportGrey)),
      child: Scaffold(
        backgroundColor: ProxmoxColors.supportBlue,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: const Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Stack(
          alignment: Alignment.center,
          children: [
            SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints.tightFor(
                    height: MediaQuery.of(context).size.height),
                child: SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        const Padding(
                          padding: EdgeInsets.fromLTRB(0, 100.0, 0, 30.0),
                          child: Icon(
                            Icons.lock,
                            size: 48,
                          ),
                        ),
                        const Text(
                          'Verify',
                          style: TextStyle(
                              fontSize: 36,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                        const Text(
                          'Check your second factor provider',
                          style: TextStyle(
                              color: Colors.white38,
                              fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 50.0, 0, 8.0),
                          child: SizedBox(
                            width: 175,
                            child: Column(
                              children: <Widget>[
                                DropdownButtonFormField(
                                  decoration: const InputDecoration(
                                      labelText: 'Method',
                                      icon: Icon(Icons.input)),
                                  items: _tfaKinds
                                      .map((e) => DropdownMenuItem(
                                            value: e,
                                            child: ListTile(title: Text(e)),
                                          ))
                                      .toList(),
                                  onChanged: (String? value) {
                                    setState(() {
                                      _selectedTfaKind = value!;
                                    });
                                  },
                                  selectedItemBuilder: (context) =>
                                      _tfaKinds.map((e) => Text(e)).toList(),
                                  value: _selectedTfaKind,
                                ),
                                TextField(
                                    controller: _codeController,
                                    textAlign: TextAlign.center,
                                    decoration: const InputDecoration(
                                        labelText: 'Code',
                                        icon: Icon(Icons.pin)),
                                    keyboardType: _selectedTfaKind == 'totp'
                                        ? TextInputType.number
                                        : TextInputType.visiblePassword,
                                    autofocus: true,
                                    onSubmitted: (value) => _submitTfaCode()),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: TextButton(
                                style: TextButton.styleFrom(
                                  foregroundColor: Colors.white,
                                  backgroundColor: const Color(0xFFE47225),
                                  disabledBackgroundColor: Colors.grey,
                                ),
                                onPressed: () => _submitTfaCode(),
                                child: const Text('Continue'),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            if (_isLoading)
              const ProxmoxProgressOverlay(
                message: 'Verifying second-factor...',
              )
          ],
        ),
      ),
    );
  }

  Future<void> _submitTfaCode() async {
    setState(() {
      _isLoading = true;
    });
    try {
      final client = await widget.apiClient!
          .finishTfaChallenge(_selectedTfaKind, _codeController.text);
      if (mounted) Navigator.of(context).pop(client);
    } on ProxmoxApiException catch (e) {
      if (mounted) {
        showDialog(
          context: context,
          builder: (context) => ProxmoxApiErrorDialog(
            exception: e,
          ),
        );
      }
    } catch (e, trace) {
      print(e);
      print(trace);
      if (mounted) {
        showDialog(
          context: context,
          builder: (context) => ConnectionErrorDialog(
            exception: e,
          ),
        );
      }
    }
    setState(() {
      _isLoading = false;
    });
  }
}
