import 'package:flutter/material.dart';
import 'package:built_collection/built_collection.dart';
import 'package:proxmox_login_manager/proxmox_general_settings_form.dart';
import 'package:proxmox_login_manager/proxmox_login_form.dart';
import 'package:proxmox_login_manager/proxmox_login_model.dart';
import 'package:proxmox_dart_api_client/proxmox_dart_api_client.dart'
    as proxclient;
import 'package:proxmox_login_manager/extension.dart';
import 'package:proxmox_login_manager/proxmox_password_store.dart';

typedef OnLoginCallback = Function(proxclient.ProxmoxApiClient client);

class ProxmoxLoginSelector extends StatefulWidget {
  final OnLoginCallback? onLogin;

  const ProxmoxLoginSelector({super.key, this.onLogin});

  @override
  State<ProxmoxLoginSelector> createState() => _ProxmoxLoginSelectorState();
}

class _ProxmoxLoginSelectorState extends State<ProxmoxLoginSelector> {
  Future<ProxmoxLoginStorage?>? loginStorage;
  @override
  void initState() {
    super.initState();
    loginStorage = ProxmoxLoginStorage.fromLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Theme.of(context).colorScheme.background,
        appBar: AppBar(
          title: const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Proxmox',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
              Text(
                'Virtual Environment',
                style: TextStyle(
                  fontSize: 14,
                ),
              )
            ],
          ),
          actions: [
            IconButton(
                icon: const Icon(Icons.settings),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const ProxmoxGeneralSettingsForm(),
                  ));
                })
          ],
        ),
        body: FutureBuilder<ProxmoxLoginStorage?>(
            future: loginStorage,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (snapshot.hasData &&
                  (snapshot.data!.logins?.isEmpty ?? true)) {
                return const Center(
                  child: Text('Add an account'),
                );
              }
              var items = <Widget>[];
              final BuiltList<ProxmoxLoginModel> logins =
                  snapshot.data?.logins ?? BuiltList<ProxmoxLoginModel>();

              final activeSessions =
                  logins.rebuild((b) => b.where((b) => b.activeSession));

              if (activeSessions.isNotEmpty) {
                items.addAll([
                  const Padding(
                    padding: EdgeInsets.all(12.0),
                    child: Text(
                      'Active Sessions',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  ...activeSessions.map((s) => ListTile(
                        title: Text(s.fullHostname),
                        subtitle: Text(s.fullUsername),
                        trailing: const Icon(Icons.navigate_next),
                        leading: PopupMenuButton(
                            icon: const Icon(Icons.more_vert,
                                color: Colors.green),
                            itemBuilder: (context) => [
                                  PopupMenuItem(
                                    child: ListTile(
                                      dense: true,
                                      leading: const Icon(Icons.logout),
                                      title: const Text('Logout'),
                                      onTap: () async {
                                        await snapshot.data!
                                            .rebuild((b) => b.logins
                                                .rebuildWhere((m) => s == m,
                                                    (b) => b..ticket = ''))
                                            .saveToDisk();
                                        refreshFromStorage();
                                        if (context.mounted) {
                                          Navigator.of(context).pop();
                                        }
                                      },
                                    ),
                                  ),
                                ]),
                        onTap: () => _login(user: s),
                      )),
                ]);
              }
              items.addAll([
                const Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    'Available Sites',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                ...logins.where((b) => !b.activeSession).map((login) =>
                    ListTile(
                      title: Text(login.fullHostname),
                      subtitle: Text(login.fullUsername),
                      trailing: const Icon(Icons.navigate_next),
                      leading: PopupMenuButton(
                          itemBuilder: (context) => [
                                if (login.passwordSaved ?? false)
                                  PopupMenuItem(
                                    child: ListTile(
                                      dense: true,
                                      leading: const Icon(Icons.key_off),
                                      title: const Text('Delete Password'),
                                      onTap: () async {
                                        await deletePassword(login.identifier!);

                                        await snapshot.data!
                                            .rebuild((b) => b
                                              ..logins.rebuildWhere(
                                                  (m) => m == login,
                                                  (b) =>
                                                      b..passwordSaved = false))
                                            .saveToDisk();
                                        refreshFromStorage();
                                        if (context.mounted) {
                                          Navigator.of(context).pop();
                                        }
                                      },
                                    ),
                                  ),
                                PopupMenuItem(
                                  child: ListTile(
                                    dense: true,
                                    leading: const Icon(Icons.delete),
                                    title: const Text('Delete'),
                                    onTap: () async {
                                      await deletePassword(login.identifier!);
                                      await snapshot.data!
                                          .rebuild(
                                              (b) => b.logins.remove(login))
                                          .saveToDisk();
                                      refreshFromStorage();
                                      if (context.mounted) {
                                        Navigator.of(context).pop();
                                      }
                                    },
                                  ),
                                ),
                              ]),
                      onTap: () => _login(user: login),
                    ))
              ]);
              return ListView(
                children: items,
              );
            }),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => _login(isCreate: true),
          label: const Text('Add'),
          icon: const Icon(Icons.account_circle),
        ),
      ),
    );
  }

  Future<void> _login({ProxmoxLoginModel? user, bool isCreate = false}) async {
    String? password;
    bool activeSession = user?.activeSession ?? false;
    String ticket = (activeSession ? user?.ticket : null) ?? '';
    bool passwordSaved = user != null && (user.passwordSaved ?? false);

    if (ticket == '' && passwordSaved) {
      password = await getPassword(user.identifier!);
    }

    if (mounted) {
      final client = await Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ProxmoxLoginPage(
                userModel: user,
                isCreate: isCreate,
                ticket: ticket,
                password: password,
              )));
      refreshFromStorage();
      if (client != null) {
        widget.onLogin!(client);
      }
    }
  }

  void refreshFromStorage() {
    setState(() {
      loginStorage = ProxmoxLoginStorage.fromLocalStorage();
    });
  }
}
