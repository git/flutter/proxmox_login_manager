import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:proxmox_login_manager/proxmox_general_settings_model.dart';
import 'package:proxmox_login_manager/proxmox_login_model.dart';
part 'serializers.g.dart';

@SerializersFor([ProxmoxLoginStorage, ProxmoxGeneralSettingsModel])
final Serializers serializers = (_$serializers.toBuilder()).build();
