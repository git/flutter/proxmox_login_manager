import 'package:biometric_storage/biometric_storage.dart';

Future<bool> canSavePassword() async {
  return await BiometricStorage().canAuthenticate() ==
      CanAuthenticateResponse.success;
}

Future<void> savePassword(String id, String password) async {
  if (await canSavePassword()) {
    BiometricStorageFile store = await BiometricStorage().getStorage(id);
    await store.write(password);
  }
}

Future<String?> getPassword(String id) async {
  String? password;
  if (await canSavePassword()) {
    BiometricStorageFile store = await BiometricStorage().getStorage(id);
    password = await store.read();
  }

  return password;
}

Future<void> deletePassword(String id) async {
  if (await canSavePassword()) {
    BiometricStorageFile store = await BiometricStorage().getStorage(id);
    await store.delete();
  }
}
