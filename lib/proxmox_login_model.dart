import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:proxmox_login_manager/proxmox_general_settings_model.dart';
import 'package:proxmox_login_manager/serializers.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:proxmox_dart_api_client/proxmox_dart_api_client.dart'
    as proxclient;
part 'proxmox_login_model.g.dart';

abstract class ProxmoxLoginStorage
    implements Built<ProxmoxLoginStorage, ProxmoxLoginStorageBuilder> {
  BuiltList<ProxmoxLoginModel>? get logins;

  ProxmoxLoginStorage._();
  factory ProxmoxLoginStorage(
          [void Function(ProxmoxLoginStorageBuilder)? updates]) =
      _$ProxmoxLoginStorage;

  Object? toJson() {
    return serializers.serializeWith(ProxmoxLoginStorage.serializer, this);
  }

  static ProxmoxLoginStorage? fromJson(Object? json) {
    return serializers.deserializeWith(ProxmoxLoginStorage.serializer, json);
  }

  static Serializer<ProxmoxLoginStorage> get serializer =>
      _$proxmoxLoginStorageSerializer;

  static Future<ProxmoxLoginStorage?> fromLocalStorage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('ProxmoxLoginList')) {
      final decodedJson = json.decode(prefs.getString('ProxmoxLoginList')!);
      return fromJson(decodedJson);
    }
    return ProxmoxLoginStorage();
  }

  Future<void> saveToDisk() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('ProxmoxLoginList', json.encode(toJson()));
  }

  Future<proxclient.ProxmoxApiClient> recoverLatestSession() async {
    final latestSession = logins!.singleWhere((e) => e.ticket!.isNotEmpty);
    final settings = await ProxmoxGeneralSettingsModel.fromLocalStorage();
    final apiClient = await proxclient.authenticate(latestSession.fullUsername,
        latestSession.ticket!, latestSession.origin!, settings.sslValidation!);
    return apiClient;
  }

  Future<void> invalidateAllSessions() async {
    final invalidatedList =
        logins!.map((e) => e.rebuild((login) => login..ticket = ''));
    await rebuild((e) => e.logins.replace(invalidatedList)).saveToDisk();
  }
}

abstract class ProxmoxLoginModel
    implements Built<ProxmoxLoginModel, ProxmoxLoginModelBuilder> {
  Uri? get origin;

  String? get username;

  String? get realm;

  bool? get passwordSaved;

  ProxmoxProductType? get productType;

  String? get ticket;

  /// The username with the corresponding realm e.g. root@pam
  String get fullUsername => '$username@$realm';

  bool get activeSession =>
      ticket != null && ticket!.isNotEmpty && !ticketExpired();

  String? get hostname;

  String get fullHostname {
    var location = origin;
    if (location == null) {
      return 'Unknown';
    }
    var name = hostname;
    if (name == null || name.isEmpty) {
      return location.host;
    }
    if (location.host == name) {
      return name;
    }
    return '${location.host} - $hostname';
  }

  String? get identifier {
    if (origin == null) {
      return null;
    }

    var host = origin!.host;
    var port = origin!.port;
    return '$username@$realm@$host:$port';
  }

  ProxmoxLoginModel._();

  factory ProxmoxLoginModel(
      [void Function(ProxmoxLoginModelBuilder)? updates]) = _$ProxmoxLoginModel;

  Map<String, dynamic>? toJson() {
    return serializers.serializeWith(ProxmoxLoginModel.serializer, this)
        as Map<String, dynamic>?;
  }

  static ProxmoxLoginModel? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(ProxmoxLoginModel.serializer, json);
  }

  static Serializer<ProxmoxLoginModel> get serializer =>
      _$proxmoxLoginModelSerializer;

  bool ticketExpired() {
    final ticketRegex = RegExp(r'(PVE|PMG)(?:QUAR)?:(?:(\S+):)?([A-Z0-9]{8})::')
        .firstMatch(ticket!)!;
    final time = DateTime.fromMillisecondsSinceEpoch(
        int.parse(ticketRegex.group(3)!, radix: 16) * 1000);
    return DateTime.now().isAfter(time.add(const Duration(hours: 1)));
  }
}

class ProxmoxProductType extends EnumClass {
  static const ProxmoxProductType pve = _$pve;
  static const ProxmoxProductType pmg = _$pmg;
  static const ProxmoxProductType pbs = _$pbs;

  const ProxmoxProductType._(super.name);

  static BuiltSet<ProxmoxProductType> get values => _$ProxmoxProductTypeValues;
  static ProxmoxProductType valueOf(String name) =>
      _$ProxmoxProductTypeValueOf(name);

  static Serializer<ProxmoxProductType> get serializer =>
      _$proxmoxProductTypeSerializer;
}
