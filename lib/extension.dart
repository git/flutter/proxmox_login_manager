import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

extension BuiltValueListBuilderExtension<V extends Built<V, B>,
    B extends Builder<V, B>> on ListBuilder<Built<V, B>> {
  void rebuildWhere(bool Function(V) test, void Function(B) updates) {
    for (var i = 0; i != length; ++i) {
      if (test(this[i] as V)) this[i] = this[i].rebuild(updates);
    }
  }
}
